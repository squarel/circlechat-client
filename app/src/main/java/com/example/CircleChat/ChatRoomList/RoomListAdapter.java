package com.example.CircleChat.ChatRoomList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.app.R;

import java.util.List;

/**
 * Created by squarel on 12/24/13.
 * This is the adapter for room list when fetching the latest available rooms from server.
 */
public class RoomListAdapter extends ArrayAdapter<Room> {
    private List<Room> items;


    public RoomListAdapter(Context context, List<Room> items) {
        super(context, R.layout.chat_room_list, items);
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(v == null) {
            LayoutInflater li = LayoutInflater.from(getContext());
            v = li.inflate(R.layout.chat_room_list, null);
        }

        Room room = items.get(position);

        if(room != null) {
            TextView titleText = (TextView)v.findViewById(R.id.title);
            TextView radiusText = (TextView)v.findViewById(R.id.radius);
            TextView coordinateText = (TextView)v.findViewById(R.id.coordinate);

            if(titleText != null) {
                titleText.setText(room.getTitle());
            }
            if(radiusText != null) {
                radiusText.setText(room.getRadius() + "m");
            }
            if(coordinateText != null) {
                coordinateText.setText(room.getLat() + ", " + room.getLng());
            }
            v.setTag(room.getRoom_id());
        }

        return v;
    }
}
