package com.example.CircleChat.ChatRoomList;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by squarel on 12/24/13.
 * This is the class for Room with basic methods and fields.
 */
public class Room {
    private String title;
    private Double lat;
    private Double lng;
    private Integer radius;
    private String room_id;

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    private String created_by;

    public Room() {

    }

    public Room(JSONObject room) {
        try {
            if(room.has("title")) title = room.getString("title");
            if(room.has("lat")) lat = room.getDouble("lat");
            if(room.has("lng")) lng = room.getDouble("lng");
            if(room.has("radius")) radius = room.getInt("radius");
            if(room.has("room_id")) room_id = room.getString("room_id");
            if(room.has("created_by")) created_by = room.getString("created_by");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
