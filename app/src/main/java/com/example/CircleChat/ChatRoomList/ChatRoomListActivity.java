package com.example.CircleChat.ChatRoomList;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.CircleChat.ChatRoom.ChatRoomActivity;
import com.example.CircleChat.ChatRoom.NewRoomActivity;
import com.example.CircleChat.SettingsActivity;
import com.example.app.R;

import java.util.List;


public class ChatRoomListActivity extends ListActivity implements FetchRoomListListener {
    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list_view);
        initView();
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Intent intent = new Intent(this, ChatRoomActivity.class);
        intent.putExtra("room_id", (String)v.getTag());
        Room room = (Room) l.getItemAtPosition(position);
        intent.putExtra("room_name", room.getTitle());
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.chat_list_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }
        if (id == R.id.action_refresh) {
            initView();
        }
        if (id == R.id.action_create) {
            Intent intent = new Intent(this, NewRoomActivity.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    private void initView() {
        dialog = ProgressDialog.show(this, "", "Loading...");

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        Float lat = Float.valueOf(sharedPref.getString("latitude", "42.2738"));
        Float lng = Float.valueOf(sharedPref.getString("longitude", "-71.031233"));
        Integer radius = Integer.valueOf(sharedPref.getString("radius", "69"));

        String url = "http://10.0.0.41:8080/?lat=" + lat + "&lng=" + lng + "&radius=" + radius;
        Log.d("line", url);
        FetchRoomListTask task = new FetchRoomListTask(this);
        task.execute(url);
    }

    @Override
    public void onFetchRoomComplete(List<Room> rooms) {
        if(dialog != null) dialog.dismiss();
        RoomListAdapter adapter = new RoomListAdapter(this, rooms);
        setListAdapter(adapter);

    }

    @Override
    public void onFetchRoomFailure(String msg) {
        if(dialog != null) dialog.dismiss();
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_chat_list_view, container, false);
            return rootView;
        }
    }

}
