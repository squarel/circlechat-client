package com.example.CircleChat.ChatRoomList;

import android.os.AsyncTask;

import com.example.CircleChat.Utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by squarel on 12/24/13.
 * This task is called when first time loading the app or refresh available room manually.
 */
public class FetchRoomListTask extends AsyncTask<String, Void, String> {
    private final FetchRoomListListener listener;
    private String msg;

    public FetchRoomListTask(FetchRoomListListener listener) {
        this.listener = listener;
    }


    @Override
    protected String doInBackground(String... params) {
        if(params == null) return null;

        String url = params[0];

        try {
            HttpClient client = new DefaultHttpClient();
            HttpGet httpget = new HttpGet(url);

            HttpResponse response = client.execute(httpget);

            HttpEntity entity = response.getEntity();

            if(entity == null) {
                msg = "No response from server";
                return null;
            }

            InputStream is = entity.getContent();
            return Utils.streamToString(is);
        }
        catch(IOException e) {
            msg = "No Network Connection";
        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        if(s == null) {
            if(listener != null) listener.onFetchRoomFailure(msg);
            return;
        }

        try {
            JSONArray aJson = new JSONArray(s);
            List<Room> rooms = new ArrayList<Room>();

            for(int i=0; i<aJson.length(); i++) {
                JSONObject json = aJson.getJSONObject(i);
                Room room = new Room(json);
                rooms.add(room);
            }

            if(listener !=null) listener.onFetchRoomComplete(rooms);
        } catch (JSONException e) {
            msg = "Invalid response";
            if(listener != null) listener.onFetchRoomFailure(msg);
            return;
        }
    }

}
