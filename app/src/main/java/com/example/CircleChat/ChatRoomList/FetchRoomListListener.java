package com.example.CircleChat.ChatRoomList;

import java.util.List;

/**
 * Created by squarel on 12/24/13.
 */
public interface FetchRoomListListener {
    public void onFetchRoomComplete(List<Room> rooms);
    public void onFetchRoomFailure(String msg);
}
