package com.example.CircleChat.ChatRoom;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * Created by squarel on 12/25/13.
 * This async task is called continuously for fetching the latest messages from server
 */
public class FetchChatTask extends AsyncTask<String, Void, ChatProtocol> {
    private final FetchChatListener listener;
    private String msg;
    private Socket socket;
    private InputStream is;

    public FetchChatTask(FetchChatListener listener, Socket socket) {
        this.listener = listener;
        this.socket = socket;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        try {
            if(socket != null)
                socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.d("line", "cancel loop");
    }

    @Override
    protected ChatProtocol doInBackground(String... params) {
        if(params == null) return null;
        if(this.isCancelled()) return null;

        String url = params[0];
        try {
            if(socket == null) {
                Log.d("line", "noooo");
                return null;
            }


            if(socket.isConnected() && ! socket.isClosed()) {
                Log.d("line", "connected");
                is = socket.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(is));
                String line;
                Log.d("line", "start loop");

                line = reader.readLine();
                Log.d("line:", line);
                ChatProtocol chat = new ChatProtocol();
                chat.fromJsonString(line);

                return chat;

            }
            Log.d("line", "not connected");

        } catch (IOException e) {
            msg = "can not connect to server";
            Log.d("line", msg);
        }
        return null;

    }

    @Override
    protected void onPostExecute(ChatProtocol c) {
        super.onPostExecute(c);
        if(!this.isCancelled()) {
            Log.d("line", "not cancelled");
            listener.onFetchChatsComplete(c);
        }
        Log.d("line", "end loop");
    }


}
