package com.example.CircleChat.ChatRoom;

import android.annotation.TargetApi;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.CircleChat.SettingsActivity;
import com.example.app.R;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ChatRoomActivity extends ListActivity implements FetchChatListener, SendMsgListener,
        ChatRoomInitListener {
    private ProgressDialog dialog;
    private ChatListAdapter adapter = null;
    private String room_id;
    FetchChatTask task = null;
    Socket socket = null;
    String url = "10.0.0.41";


    @Override
    protected void onStop() {
        super.onStop();
        if(task != null)
            task.cancel(true);
        if(socket != null)
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_room);
        initView();

    }

    private void initView() {
        //dialog = ProgressDialog.show(this, "", "Loading history messages");
        Log.d("line", "here1");
        Bundle extras = getIntent().getExtras();
        room_id = extras.getString("room_id");
        String room_name = extras.getString("room_name");
        Log.d("line", room_name);
        setTitle(room_name);

        List<ChatProtocol> lc = new ArrayList<ChatProtocol>();
        adapter = new ChatListAdapter(this, lc); //history msgs
        setListAdapter(adapter);

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String user_name = sharedPref.getString("user_name", "foo");
        ChatRoomInitTask task = new ChatRoomInitTask(this, socket, room_id, user_name);
        task.execute(url);

        Log.d("line", "done");

        this.task = new FetchChatTask(this, socket);
        this.task.execute(url);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.chat_room, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onFetchChatsComplete(ChatProtocol chat) {
        if(dialog != null) dialog.dismiss();
        adapter.add(chat);
        adapter.notifyDataSetChanged();
        this.setSelection(adapter.getCount() - 1);
        if(socket != null && ! socket.isClosed()) {
            task = new FetchChatTask(this, socket);
            task.execute(url);
        }
    }


    @Override
    public void onFetchChatsFailure(String result) {

    }

    @Override
    public void onSendMsgComplete(String result) {
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSendMsgFailure(String result) {
        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void sendMsg(View view) {
        Log.d("send", "event");
        EditText et = (EditText) findViewById(R.id.editTextMsg);
        String msg = et.getText().toString();
        et.setText("");
        if(msg != "") {
            ChatProtocol chat = new ChatProtocol(msg);
            Log.d("send", chat.toJsonString());
            SendMsgTask task = new SendMsgTask(this, socket, chat.toJsonString());
            Log.d("send", "create send task");
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, url);
            Log.d("send", "finish send task");
        }
    }

    @Override
    public void onInitRoomComplete(String s, Socket socket) {
        this.socket = socket;
        if(socket != null && ! socket.isClosed()) {
            Log.d("socket", socket.toString());
            this.task = new FetchChatTask(this, this.socket);
            this.task.execute(url);
        }
    }

    @Override
    public void onInitRoomFailure(String s) {

    }
}
