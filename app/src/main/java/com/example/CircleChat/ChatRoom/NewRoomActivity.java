package com.example.CircleChat.ChatRoom;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.CircleChat.ChatRoomList.Room;
import com.example.app.R;

public class NewRoomActivity extends Activity implements NewRoomListener {

    private int radius;
    private Float latitude;
    private Float longitude;
    private String title;
    private String user_name;
    private String url = "http://10.0.0.41:8080";
    TextView showRadius;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_room);
        SeekBar seekbar = (SeekBar) findViewById(R.id.seekRadius);
        showRadius = (TextView) findViewById(R.id.labelRadiusShow);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        user_name = sharedPref.getString("user_name", "foo");

        seekbar.setOnSeekBarChangeListener( new SeekBar.OnSeekBarChangeListener()
        {
            public void onProgressChanged(SeekBar seekBar, int progress,
                                          boolean fromUser)
            {
                // TODO Auto-generated method stub
                radius = progress;
                showRadius.setText(radius + "m");
            }

            public void onStartTrackingTouch(SeekBar seekBar)
            {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar)
            {
                // TODO Auto-generated method stub
            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.new_room, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onNewRoomComplete(String room_id) {
        Toast.makeText(this, "room created", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, ChatRoomActivity.class);
        intent.putExtra("room_id", room_id);
        intent.putExtra("room_name", this.title);
        startActivity(intent);
    }

    @Override
    public void onNewRoomFailure(String res) {
        Toast.makeText(this, res, Toast.LENGTH_SHORT).show();
    }

    public void createRoom(View view) {
        EditText textTitle = (EditText) findViewById(R.id.textRoomName);
        EditText textLat = (EditText) findViewById(R.id.textLat);
        EditText textLng = (EditText) findViewById(R.id.textLng);
        this.title = textTitle.getText().toString();
        this.latitude = Float.valueOf(textLat.getText().toString());
        this.longitude = Float.valueOf(textLng.getText().toString());
        Room new_room = new Room();
        new_room.setTitle(this.title);
        new_room.setLat(Double.valueOf(this.latitude.toString()));
        new_room.setLng(Double.valueOf(this.longitude.toString()));
        new_room.setRadius(this.radius);
        new_room.setCreated_by(this.user_name);
        NewRoomTask task = new NewRoomTask(this, this.url);
        task.execute(new_room);
    }
}
