package com.example.CircleChat.ChatRoom;

import android.text.format.Time;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by squarel on 12/25/13.
 * This class is the basic serialization of
 */
public class ChatProtocol {
    private String content = null;
    private String sender = null;
    private String room_id = null;
    private String time = null;
    private String join_name = null;
    private String leave_name = null;

    public void fromJsonString(String json_str) {
        try {
            JSONObject msg = new JSONObject(json_str);
            if(msg.has("msg")) content = msg.getString("msg");
            if(msg.has("name")) sender = msg.getString("name");
            if(msg.has("iam")) join_name = msg.getString("iam");
            if(msg.has("out")) leave_name = msg.getString("out");
            if(msg.has("time")) time = (msg.getString("time"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public ChatProtocol() {

    }

    public ChatProtocol(String msg) {
        this.content = msg;
        this.time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }

    public ChatProtocol(String room_id, String sender) {
        this.room_id = room_id;
        this.sender = sender;
        this.time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
    }

    public String toJsonString() {
        JSONObject msg = new JSONObject();
        try {
            if(content != null) msg.put("msg", content);
            if(sender != null) msg.put("name", sender);
            if(join_name != null) msg.put("iam", join_name);
            if(leave_name != null) msg.put("out", leave_name);
            if(room_id != null) msg.put("room", room_id);
            if(time != null) msg.put("time", time);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return msg.toString();
    }

    @Override
    public String toString() {
        if(join_name !=null)
            return join_name + " join the room";
        else if(leave_name != null)
            return leave_name + " leave the room";
        else
            return sender + ": " + content;
    }


    public String getTime() {
        return time;
    }

}
