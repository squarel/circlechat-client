package com.example.CircleChat.ChatRoom;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Created by squarel on 12/25/13.
 * This task is for sending a message to server
 */
public class SendMsgTask extends AsyncTask<String, Void, String> {
    private final SendMsgListener listener;
    private String res;
    private String msg;
    private Socket socket;
    private OutputStream os;

    public SendMsgTask(SendMsgListener listener, Socket socket, String msg) {
        this.listener = listener;
        this.socket = socket;
        this.msg = msg;
    }

    @Override
    protected String doInBackground(String... params) {
        Log.d("send", "start task");
        if(params == null) return null;

        String url = params[0];
        try {
            if(socket == null) {
                Log.d("send", "nooooooo");
                return null;
            }

            if(socket.isConnected()) {
                Log.d("send", "connected");
                os = socket.getOutputStream();
                os.write(msg.getBytes());
                res = "send successful";
                return res;
            }
            Log.d("send", "not connected");

        } catch (IOException e) {
            res = "can not connect to server";
            Log.d("send", res);
        }
        return null;

    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(s == null) {
            listener.onSendMsgFailure("can not connect");
        }
        else {
            listener.onSendMsgComplete(s);
        }
    }
}
