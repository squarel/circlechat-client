package com.example.CircleChat.ChatRoom;

import android.os.AsyncTask;
import android.util.Log;

import com.example.CircleChat.ChatRoomList.Room;
import com.example.CircleChat.Utils;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by squarel on 1/1/14.
 * This task is called when creating a new room
 */
public class NewRoomTask extends AsyncTask<Room, Void, String> {
    private final NewRoomListener listener;
    private String msg = null;
    private final String url;


    public NewRoomTask(NewRoomListener listener, String url) {
        this.listener = listener;
        this.url = url;
    }
    @Override
    protected String doInBackground(Room... params) {
        if(params == null) return null;
        // Create a new HttpClient and Post Header
        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(this.url);
        Room room = params[0];

        try {
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(5);
            nameValuePairs.add(new BasicNameValuePair("user", room.getCreated_by()));
            nameValuePairs.add(new BasicNameValuePair("title", room.getTitle()));
            nameValuePairs.add(new BasicNameValuePair("lat", room.getLat().toString()));
            nameValuePairs.add(new BasicNameValuePair("lng", room.getLng().toString()));
            nameValuePairs.add(new BasicNameValuePair("radius", room.getRadius().toString()));
            httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            Log.d("new", "prepare to post");
            HttpResponse response = httpclient.execute(httppost);
            Log.d("new", "get response");
            HttpEntity entity = response.getEntity();
            Log.d("new", "get response entity");
            if(entity == null) {
                msg =  "No response from server";
                return null;
            }

            InputStream is = entity.getContent();

            // return room id
            return Utils.streamToString(is);


        } catch (ClientProtocolException e) {
            msg =  "can not connect";
            return null;
        } catch (IOException e) {
            msg = "io can not connect";
            return null;
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(s == null) {
            listener.onNewRoomFailure(msg);
        }
        else if(s == "-1") {
            listener.onNewRoomFailure("server failed");
        }
        else {
            Log.d("new", s);
            listener.onNewRoomComplete(s);
        }

    }
}
