package com.example.CircleChat.ChatRoom;

import java.net.Socket;

/**
 * Created by squarel on 12/29/13.
 */
public interface ChatRoomInitListener {
    public void onInitRoomComplete(String s, Socket socket);
    public void onInitRoomFailure(String s);
}
