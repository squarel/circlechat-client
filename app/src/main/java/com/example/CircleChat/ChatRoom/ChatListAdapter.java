package com.example.CircleChat.ChatRoom;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.app.R;

import java.util.List;

/**
 * Created by squarel on 12/25/13.
 * This is the adapter for chatting interface
 */
public class ChatListAdapter extends ArrayAdapter<ChatProtocol> {
    private List<ChatProtocol> items;

    public ChatListAdapter(Context context, List<ChatProtocol> items) {
        super(context, R.layout.chat_list, items);
        this.items = items;
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if(v == null) {
            LayoutInflater li = LayoutInflater.from(getContext());
            v = li.inflate(R.layout.chat_list, null);
        }

        ChatProtocol chat = items.get(position);

        if(chat != null) {
            TextView titleText = (TextView)v.findViewById(R.id.chat_content);

            if(titleText != null) {
                titleText.setText(chat.getTime() + "  " + chat.toString());
            }
        }

        return v;
    }
}
