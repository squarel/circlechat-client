package com.example.CircleChat.ChatRoom;

import java.net.Socket;

/**
 * Created by squarel on 12/25/13.
 */
public interface FetchChatListener {
    public void onFetchChatsComplete(ChatProtocol chat);
    public void onFetchChatsFailure(String msg);
}
