package com.example.CircleChat.ChatRoom;

/**
 * Created by squarel on 12/26/13.
 */
public interface SendMsgListener {
    public void onSendMsgComplete(String result);
    public void onSendMsgFailure(String result);
}
