package com.example.CircleChat.ChatRoom;

/**
 * Created by squarel on 1/1/14.
 */
public interface NewRoomListener {
    public void onNewRoomComplete(String room_id);
    public void onNewRoomFailure(String res);
}
