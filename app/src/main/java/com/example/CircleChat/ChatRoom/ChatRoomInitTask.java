package com.example.CircleChat.ChatRoom;

import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Created by squarel on 12/29/13.
 * This task should be first called when enter a room.
 * It will provide the room_id and user name to server.
 */
public class ChatRoomInitTask extends AsyncTask<String, Void, String> {
    private final ChatRoomInitListener listener;
    private String msg;
    private Socket socket;
    private OutputStream os;
    private String room_id;
    private String name;
    private String res;


    public ChatRoomInitTask(ChatRoomInitListener listener, Socket socket, String room_id, String name) {
        this.listener = listener;
        this.socket = socket;
        this.room_id = room_id;
        this.name = name;
    }

    @Override
    protected String doInBackground(String... params) {
        Log.d("send", "start init room");
        if(params == null) return null;

        String url = params[0];

        try {
            socket = new Socket(url, 80);
            if(socket.isConnected()) {
                Log.d("send", "connected");
                os = socket.getOutputStream();
                ChatProtocol init_msg = new ChatProtocol(room_id, name);
                os.write(init_msg.toJsonString().getBytes());
                res = "init success";
                return res;

            }
        } catch (IOException e) {
            res = "can not initialize room";
            Log.d("send", res);
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(s == null) {
            listener.onInitRoomFailure("can not connect");
        }
        else {
            listener.onInitRoomComplete(s, socket);
        }
    }
}
