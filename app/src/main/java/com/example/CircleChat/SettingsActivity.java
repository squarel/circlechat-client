package com.example.CircleChat;

import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Settings activity.
 * Due to Google Maps V2 not available on emulator, this settings contains setup the coordinate of
 * device manually.
 */

public class SettingsActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }
}
